﻿using UnityEngine;
using System.Collections;

public class MoveChar : MonoBehaviour {

	Rigidbody2D myrb;
	float xspeed;
	float yspeed;

	void Start () {
		myrb = this.GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate()
	{
		myrb.velocity = new Vector2 (xspeed, yspeed);
	}

	void Update () {
		if (Input.GetKey (KeyCode.RightArrow)) {
			xspeed = 5;
		} else if (Input.GetKey (KeyCode.LeftArrow))
		{
			xspeed = -5;
		} else
		{
			xspeed = 0;
		}

		if (Input.GetKey (KeyCode.UpArrow)) {
			yspeed = 5;
		} else if (Input.GetKey (KeyCode.DownArrow))
		{
			yspeed = -5;
		} else
		{
			yspeed = 0;
		}
	}
}